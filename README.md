# Clock
## Version 1.3
## © Xecestel
## MIT License

## Overview
Made with Godot Engine 3.2.
This module is a simple scene containing a clock you can add to your scenes to count the time.  
By design it can be used either as a clock or as a stopwatch for your games, depending of what you need.  
Just add it on the scene and it will start counting in background.  

## Instructions
This clock by default counts the hours, the minutes, the seconds and the milliseconds. However, you can decide what you want the player to see, hiding what you don't need from the label. You can also hide the label completely and just use the built-in getters to get information about time to use in other ways.  
The clock by default inherits the Pause properties, but you can also pause it manually if you need to using the `pause()` and `unpause()` methods. The `is_paused` method lets you check if the clock is paused or not.  
You can also add a `Signal_time` to your clock. What's a signal time? If the clock passes the specified time, it fires a `time_passed` signal you can catch. This can be useful to make day-night cycles and in general every time you want to do something when a certain time passes. The `Signal_times` Dictionary can contain all the signal times you want. Just make sure to respect the default format (string key, dictionary value with int "Hours", int "Minutes" and float "Seconds" entries). You can also call them however you like, as when the signal fires it will bring the id with it!  
You can also manually set a time for the clock.  
If you want the time to run faster or slower than real time, you can set the `time_speed` property. The default value it's 1.0.    
All the properties can be set from the `Clock` root scene properties in the editor.  
Use the `lap()` method to stop and save a lap on the stopwatch! If the label is visible, it will also appear on the screen.  

## Objectives for the future
Currently none

## Credits
Designed and programmed by Celeste Privitera (@xecestel).

## Licenses
Clock  
Copyright © 2020 Celeste Privitera  
This software is an open source project licensed under MIT License. Check the LICENSE file for more info.  

## Changelog

### Version 1.0
- Initial Commit

### Version 1.1
- Added laps

### Version 1.2
- Laps are now visible on screen
- You can now change the label's font from the scene properties

### Version 1.2.1
- Now you can hide laps even with the label visible
- Improved code and readability

### Version 1.3
- The scene root is now the VBoxContainer containing the clock and the laps: this gives the user more control over the position of the clock
