extends HBoxContainer

####################################################################
#	TIMELABEL SCRIPT FOR THE CLOCK MODULE FOR GODOT 3.2
#			Version 1.3
#			© Xecestel
####################################################################
#
# This software is licensed under the MIT License conditions.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#####################################################################


# Variables

var show_milliseconds : bool = false

############

# Create the time label
func make_label(show_hours : bool, show_minutes : bool, show_seconds : bool, show_milliseconds : bool, time : Time_Class = null, custom_font : DynamicFont = null) -> void:
	self.set_visibility(show_hours, show_minutes, show_seconds, show_milliseconds)
	self.set_font(custom_font)
	self.set_time(time)


# Sets the label's visibility
func set_visibility(show_hours : bool, show_minutes : bool, show_seconds : bool, show_milliseconds : bool) -> void:
	self.show_milliseconds = show_milliseconds
	self.get_node("Hours").set_visible(show_hours)
	self.get_node("Divisor").set_visible(show_hours && show_minutes)
	self.get_node("Minutes").set_visible(show_minutes)
	self.get_node("Divisor2").set_visible(show_minutes && show_seconds)
	self.get_node("Seconds").set_visible(show_seconds)


# Sets the label's font
func set_font(font : DynamicFont) -> void:
	if font != null:
		for child in self.get_children():
			child.set("custom_fonts/font", font)


func set_time(time : Time_Class) -> void:
	if time != null:
		self.get_node("Hours").set_text(str("%02d" % time.get_hours()))
		self.get_node("Minutes").set_text(str("%02d" % time.get_minutes()))
		if show_milliseconds:
			self.get_node("Seconds").set_text(str("%06.3f" % time.get_seconds()))
		else:
			self.get_node("Seconds").set_text(str("%02d" % time.get_Seconds()))


#########################
#	SETTERS & GETTERS	#
#########################

# Sets if the milliseconds are to be shown
func set_show_milliseconds(show : bool) -> void:
	self.show_milliseconds = show


# Sets if the seconds are to be shown
func set_show_seconds(show : bool) -> void:
	self.show_seconds = show
	self.get_node("Seconds").set_visible(show)


# Sets if the minutes are to be shown
func set_show_minutes(show : bool) -> void:
	self.show_minutes = show
	self.get_node("Minutes").set_visible(show)


# Sets if the hours are to be shown
func set_show_hours(show : bool) -> void:
	self.show_hours = show
	self.get_node("Hours").set_visible(show)


# Sets if the label is to be shown
func set_show_label(show : bool) -> void:
	self.show_label = show
	self.set_visible(show)


# Returns true if the milliseconds are to be shown
func is_show_milliseconds() -> bool:
	return self.show_milliseconds


# Returns true if the seconds are to be shown
func is_show_seconds() -> bool:
	return self.show_seconds


# Returns true if the minutes are to be shown
func is_show_minutes() -> bool:
	return self.show_minutes


# Returns true if the hours are to be shown
func is_show_hours() -> bool:
	return self.show_hours


# Returns true if the label is to be shown
func is_show_label() -> bool:
	return self.show_label
