class_name Time_Class


####################################################################
#	TIME_CLASS SCRIPT FOR THE CLOCK MODULE FOR GODOT 3.2
#			Version 1.3
#			© Xecestel
####################################################################
#
# This software is licensed under the MIT License conditions.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#####################################################################


# Variables

var hours : int = 0
var minutes : int = 0
var seconds : float = 0.000

############

# Constructor for the class
func _init(time : Dictionary = {}, hours : int = 0, minutes : int = 0, seconds : float = 0.0) -> void:
	if time.empty():
		self.set_hours(hours)
		self.set_minutes(minutes)
		self.set_seconds(seconds)
	else:
		self.set_time(time)


# Resets the clock
func reset_time() -> void:
	self.set_hours(0)
	self.set_minutes(0)
	self.set_seconds(0)


# Add a specific amount to the hours
func add_hours(how_much : int) -> void:
	self.hours += how_much


# Add a specific amunt to the minutes
func add_minutes(how_much : int) -> void:
	self.minutes += how_much


# Add a specific amount to the seconds
func add_seconds(how_much : float) -> void:
	self.seconds += how_much


# Subtract a specific amount to the hours
func subtract_hours(how_much : int) -> void:
	self.hours -= how_much


# Subtract a specific amount to the minutes
func subtract_minutes(how_much : int) -> void:
	self.minutes -= how_much


# Subtract a specific amount to the seconds
func subtract_seconds(how_much : float) -> void:
	self.seconds -= how_much


# Compares two Time_Class objects
func compare_with_time(time2 : Time_Class) -> bool:
	return (self.get_hours() == time2.get_hours() &&
			self.get_minutes() == time2.get_minutes() &&
			self.get_seconds() == time2.get_seconds())


# Add all the time properties with time2
func add_time(time2 : Time_Class) -> void:
	self.add_hours(time2.get_hours())
	self.add_minutes(time2.get_hours())
	self.add_seconds(time2.get_seconds())


# Subtract all the time properties of time2 from self
func subtract_time(time2 : Time_Class) -> void:
	self.subtract_hours(time2.get_hours())
	self.subtract_minutes(time2.get_hours())
	self.subtract_seconds(time2.get_seconds())


#########################
#	SETTERS & GETTERS	#
#########################

# Sets the time from a dictionary
func set_time(time : Dictionary) -> void:
	for key in time.keys():
		self.set(key.to_lower(), time[key])


# Sets the hours
func set_hours(hours : int) -> void:
	self.hours = hours


# Returns the hours
func get_hours() -> int:
	return self.hours


# Sets the minutes
func set_minutes(minutes : int) -> void:
	self.minutes = minutes


# Returns the minutes
func get_minutes() -> int:
	return self.minutes


# Sets the seconds
func set_seconds(seconds : float) -> void:
	self.seconds = seconds


# Returns the seconds
func get_seconds() -> float:
	return self.seconds
