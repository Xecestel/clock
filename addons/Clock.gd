extends VBoxContainer

####################################################################
#	CLOCK MODULE FOR GODOT 3.2
#			Version 1.3
#			© Xecestel
####################################################################
#
# This software is licensed under the MIT License conditions.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#####################################################################


# Variables

var Time : Time_Class

export (Dictionary) var Signal_times = {
	"Default" : {
		"Hours" : 0,
		"Minutes" : 0,
		"Seconds" : 0.0
	}
}

export (float) var time_speed = 1.0
export (int) var time_format = 24
export (bool) var show_label = true
export (bool) var show_laps = true
export (bool) var show_milliseconds = true
export (bool) var show_seconds = true
export (bool) var show_minutes = true
export (bool) var show_hours = true
export (DynamicFont) var custom_font = null

var paused = false
var laps : Array

onready var time = self.get_node("TimeLabel")
onready var lap_container = self.get_node("Laps")

#########################


# Signals

signal time_passed(time_id)
signal time_elapsed()

#############


#########################
#	UTILITY METHODS		#
#########################

# Pause the clock
func pause() -> void:
	self.paused = true
	self.set_process(false)


# Resumes the clock
func resume() -> void:
	self.paused = false
	self.set_process(true)


# Resets the clock
func reset_time() -> void:
	Time.reset_time()


# Stops a lap and saves it on the laps array
func lap() -> void:
	var Lap : Dictionary = {
		"Total Time" : Time_Class.new(),
		"Lap Time" : Time_Class.new()
	}
	
	Lap["Total Time"].set_time(inst2dict(Time))
	Lap["Lap Time"].set_time(inst2dict(Time))
	if laps.empty() == false:
		Lap["Lap Time"].subtract_time(laps[laps.size() - 1]["Lap Time"])
	laps.append(Lap)
	self.add_lap_label()


#########################
#	SETTERS & GETTERS	#
#########################

# Sets the time from a dictionary
func set_time(time : Dictionary) -> void:
	Time.set_time(time)


# Returns time as a dictionary
func get_time() -> Time_Class:
	return Time


# Sets a signal time
func set_signal_time(time_id : String, value : Dictionary) -> void:
	Signal_times[time_id] = value


# Returns a specified signal time
func get_signal_time(time_id : String) -> Dictionary:
	return Signal_times.get(time_id)


# Returns time in format HH:MM:SS.SSS
func get_formatted_time() -> String:
	var string = str("%02d" % Time.get_hours()) + ":"
	string += str("%02d" % Time.get_minutes()) + ":"
	string += str("%06.3d" % Time.get_seconds()) if show_milliseconds else str("%02d" % Time.get_seconds())
	return string


# Sets the hours
func set_hours(hours : int) -> void:
	Time.set_hours(hours)


# Returns the hours
func get_hours() -> int:
	return Time.get_hours()


# Sets the minutes
func set_minutes(minutes : int) -> void:
	Time.set_minutes(minutes)


# Returns the minutes
func get_minutes() -> int:
	return Time.get_minutes()


# Sets the seconds
func set_seconds(seconds : float) -> void:
	Time.set_seconds(seconds)


# Returns the seconds
func get_seconds() -> float:
	return Time.get_seconsd()


# Sets the duration of a second
func set_time_speed(speed : float) -> void:
	self.time_speed = speed


# Sets the time format
func set_time_format(format : int) -> void:
	self.time_format = format


# Sets if the milliseconds are to be shown
func set_show_milliseconds(show : bool) -> void:
	self.show_milliseconds = show
	time.set_show_milliseconds(show)


# Sets if the seconds are to be shown
func set_show_seconds(show : bool) -> void:
	self.show_seconds = show
	time.set_show_seconds(show)


# Sets if the minutes are to be shown
func set_show_minutes(show : bool) -> void:
	self.show_minutes = show
	time.set_show_minutes(show)


# Sets if the hours are to be shown
func set_show_hours(show : bool) -> void:
	self.show_hours = show
	time.set_show_hours(show)


# Sets if the label is to be shown
func set_show_label(show : bool) -> void:
	self.show_label = show
	time.set_show_label(show)


# Returns the duration of a second
func get_time_speed() -> float:
	return self.time_speed


# Returns the time format
func get_time_format() -> int:
	return self.time_format


# Returns true if the milliseconds are to be shown
func is_show_milliseconds() -> bool:
	return self.show_milliseconds


# Returns true if the seconds are to be shown
func is_show_seconds() -> bool:
	return self.show_seconds


# Returns true if the minutes are to be shown
func is_show_minutes() -> bool:
	return self.show_minutes


# Returns true if the hours are to be shown
func is_show_hours() -> bool:
	return self.show_hours


# Returns true if the label is to be shown
func is_show_label() -> bool:
	return self.show_label


# Returns if the clock is paused
func is_paused() -> bool:
	return paused


#########################
#	INTERNAL METHODS	#
#########################

# Called when the node enters the scene tree for the first time
func _ready() -> void:
	self.init_labels()
	self.init_times()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	Time.add_seconds(delta * time_speed)
	if Time.get_seconds() >= 60.0 && self.show_minutes:
		Time.subtract_seconds(60.0)
		Time.add_minutes(1)
	if Time.get_minutes() >= 60 && self.show_hours:
		Time.subtract_minutes(60)
		Time.add_hours(1)
	if Time.get_hours() >= time_format:
		Time.set_hours(0)
	
	self.check_passed_time()
	
	if show_label:
		self.update_time()


# Updates time in label
func update_time() -> void:
	time.set_time(Time)


# Initilizes the label showing the needed parts
func init_labels() -> void:
	time.set_visible(show_label)
	lap_container.set_visible(show_laps)
	if show_label:
		time.make_label(show_hours, show_minutes, show_seconds, show_milliseconds, custom_font)


# Initialize Time objects
func init_times() -> void:
	Time = Time_Class.new()
	for time in Signal_times.keys():
		Signal_times[time] = Time_Class.new(Signal_times[time])


# Checks if it's time to fire the signal
func check_passed_time() -> void:
	if !Signal_times.empty():
		for time in Signal_times.keys():
			var signal_time = Signal_times[time]
			if (Time.get_hours() == signal_time.get_hours() &&
				Time.get_minutes() == signal_time.get_minutes() &&
				Time.get_seconds() >= signal_time.get_seconds() &&
				Time.get_seconds() <= signal_time.get_seconds() + 0.019 * time_speed):
					emit_signal("time_passed", time)
					self.lap()


# Adds a lap label to the scene
func add_lap_label() -> void:
	if show_label && show_laps:
		# Making sure the container is visible
		lap_container.set_visible(true)
		
		# Initializing all nodes
		var lap_box = HBoxContainer.new() # This will contain the lap info
		var time_label_scene = load("res://Clock/Classes/TimeLabel.tscn")
		var lap_time_node = time_label_scene.instance() # This will contain the lap time
		var total_time_node = time_label_scene.instance() # This will contain the total time
		var lap_counter = Label.new() #This will contain the lap counter
		
		# Making the labels
		lap_time_node.make_label(show_hours, show_minutes, show_seconds, show_milliseconds, laps[laps.size() - 1]["Lap Time"], custom_font)
		total_time_node.make_label(show_hours, show_minutes, show_seconds, show_milliseconds, laps[laps.size() - 1]["Total Time"], custom_font)
		lap_counter.set_text(str(laps.size()) + " - ")
		
		# Adding the nodes to the scene
		lap_box.add_child(lap_counter)
		lap_box.add_child(lap_time_node)
		lap_box.add_child(total_time_node)
		lap_container.add_child(lap_box)
